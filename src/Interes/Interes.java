package Interes;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Scanner;

public class Interes {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.UK);
        System.out.print("introduce el capital: ");
        double capital = entrada.nextDouble();
        System.out.print("introduce el porcentaje de interes anual: ");
        double porcentaje = entrada.nextDouble();
        System.out.print("introduce el número de años: ");
        int años = entrada.nextInt();
        double interesesTotales = 0;
        int elAño = 1;
        while (elAño <= años) {
            elAño++;
            interesesTotales += getIntereses(capital, porcentaje);
            capital = capital + getIntereses(capital, porcentaje);
            System.out.printf("año: %d > interes = %.2f capital = %.2f\n",
            elAño-1, getIntereses(capital, porcentaje), capital);
        }
        System.out.println("has ganado " + interesesTotales + "€");
    }
    private static double getIntereses(double capital, double porcentaje) {
        return capital * porcentaje / 100;
    }
}